
window.onload = starter;

function starter() {
    var element = document.getElementById('box');
    element.style.backgroundColor = "red";
    element.style.width = "700px";
    element.style.height = "700px";
    element.style.border = "1px";
}

function fetchColours() {
    colourList = colourDisplay();
    var list = colourList.colours;
    i = 0;
    var html = "<select id='select' > ";
    for (var key in list) {
        i++;
        if (list.hasOwnProperty(key)) {
            html += "<option id = " + i + ">" + key + "</option>";
        }
    }
    html += "</select><br> ";
    var element = document.getElementById('col');
    element.insertAdjacentHTML('beforeend', html);
    var changeColourButton = document.getElementById('enter');
    changeColourButton.style.display = "block";
    var fetchColourButton = document.getElementById('submit');
    fetchColourButton.style.display = "none";
}


function changeColour() {
    var selectElement = document.getElementById("select");
    var strUser = selectElement.options[selectElement.selectedIndex].text;
    var changeColourButton = document.getElementById('enter');
    var colourCode = fetchCode(strUser);
    var element = document.getElementById('box');
    element.style.backgroundColor = colourCode;
}